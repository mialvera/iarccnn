# split into train and test set
from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import expand_dims
from numpy import asarray
from mrcnn.utils import Dataset
from mrcnn.visualize import display_instances
from mrcnn.utils import extract_bboxes
import matplotlib.pyplot as plt
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.utils import compute_ap
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image

# class that defines and loads the kangaroo dataset
class GunDataset(Dataset):
    # load the dataset definitions
    def load_dataset(self, dataset_dir, is_train=True):
        # define one class
        self.add_class("dataset", 1, "gun")
        # define data locations
        images_dir = dataset_dir + '/images/'
        annotations_dir = dataset_dir + '/annots/'
        # find all images
        #bad = ['230', '240','234', '246','233','37','64','32','63','164','51','3','15']
        for filename in listdir(images_dir):
            # extract image id
            image_id = filename[4:-4]

            #if image_id in bad:
            #    continue
            # skip all images after 150 if we are building the train set
            if is_train and int(image_id) >= 230:
                continue
            # skip all images before 150 if we are building the test/val set
            if not is_train and int(image_id) < 230:
                continue
            img_path = images_dir + filename
            ann_path = annotations_dir +'gun_' + image_id + '.xml'
            # add to dataset
            self.add_image('dataset', image_id=image_id, path=img_path, annotation=ann_path)


    # load the masks for an image
    def load_mask(self, image_id):
        # get details of image
        info = self.image_info[image_id]
        # define box file location
        path = info['annotation']
        # load XML
        boxes, w, h = self.extract_boxes(path)
        # create one array for all masks, each on a different channel
        masks = zeros([h, w, len(boxes)], dtype='uint8')
        # create masks
        class_ids = list()
        for i in range(len(boxes)):
            box = boxes[i]
            row_s, row_e = box[1], box[3]
            col_s, col_e = box[0], box[2]
            masks[row_s:row_e, col_s:col_e, i] = 1
            class_ids.append(self.class_names.index('gun'))
        return masks, asarray(class_ids, dtype='int32')


    # load an image reference
    def image_reference(self, image_id):
        info = self.image_info[image_id]
        return info['path']

    # function to extract bounding boxes from an annotation file
    def extract_boxes(self,filename):
        # load and parse the file
        tree = ElementTree.parse(filename)
        # get the root of the document
        root = tree.getroot()
        # extract each bounding box
        boxes = list()
        for box in root.findall('.//bndbox'):
            xmin = int(box.find('xmin').text)
            ymin = int(box.find('ymin').text)
            xmax = int(box.find('xmax').text)
            ymax = int(box.find('ymax').text)
            coors = [xmin, ymin, xmax, ymax]
            boxes.append(coors)
        # extract image dimensions
        width = int(root.find('.//size/width').text)
        height = int(root.find('.//size/height').text)
        return boxes, width, height


# train set
train_set = GunDataset()
train_set.load_dataset('Gun', is_train=True)
train_set.prepare()
print('Train: %d' % len(train_set.image_ids))

# test/val set
test_set = GunDataset()
test_set.load_dataset('Gun', is_train=False)
test_set.prepare()
print('Test: %d' % len(test_set.image_ids))

# plot first few images
for i in range(9):
    # define subplot
    plt.subplot(330 + 1 + i)
    # plot raw pixel data
    image = train_set.load_image(i)
    plt.imshow(image)
    # plot all masks
    mask, _ = train_set.load_mask(i)
    for j in range(mask.shape[2]):
        plt.imshow(mask[:, :, j], cmap='gray', alpha=0.3)
    # show the figure
plt.show()


# # enumerate all images in the dataset
# for image_id in train_set.image_ids:
#     # load image info
#     info = train_set.image_info[image_id]
#     # display on the console
#     print(info)
#

# # define image id
# image_id = 1
# # load the image
# image = train_set.load_image(image_id)
# # load the masks and the class ids
# mask, class_ids = train_set.load_mask(image_id)
# # extract bounding boxes from the masks
# bbox = extract_bboxes(mask)
# # display image with masks and bounding boxes
# display_instances(image, bbox, mask, class_ids, train_set.class_names)
