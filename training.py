from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import expand_dims
from numpy import asarray
from mrcnn.utils import Dataset
from mrcnn.visualize import display_instances
from mrcnn.utils import extract_bboxes
import matplotlib.pyplot as plt
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.utils import compute_ap
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image

'''
	File for doind the training using the model
	Mask-RCNN
	
	needed:
		** Gun dataset on directory ./Gun/images and ./Gun/annots 
		** Weights for model pre-fit "mask_rcnn_coco.h5"
'''

## Clas needed for the model MaskRCNN to load the dataset
class GunDataset(Dataset):
	# rewrite function to load dataset
	def load_dataset(self, dataset_dir, is_train=True):
		# add class gun dataset
		self.add_class("dataset", 1, "gun")
		# list all images
		for filename in listdir(dataset_dir + '/images/'):
			# get image id, number increasing
			imageId = filename[4:-4]
			# ignore images for training set, the is 230 because the indixes ar not completely continuos
			if is_train and int(imageId) >= 230:
				continue
			# from 230 they are testing set
			if not is_train and int(imageId) < 230:
				continue
			img = dataset_dir + '/images/' + filename
			ann = dataset_dir + '/annots/' +'gun_' + imageId + '.xml'
			# add to dataset
			self.add_image('dataset', image_id=imageId, path=img, annotation=ann)


	# load the masks for an image
	def load_mask(self, image_id):
		# get info of image
		info = self.image_info[image_id]
		# define box file location
		path = info['annotation']
		# get bounding bixes
		boxes, w, h = self.extract_boxes(path)
		# create one array for all masks, each on a different channel
		masks = zeros([h, w, len(boxes)], dtype='uint8')
		# create masks
		class_ids = list()
		for i in range(len(boxes)):
			box = boxes[i]
			row_s, row_e = box[1], box[3]
			col_s, col_e = box[0], box[2]
			masks[row_s:row_e, col_s:col_e, i] = 1
			class_ids.append(self.class_names.index('gun'))
		return masks, asarray(class_ids, dtype='int32')


	# load an image reference
	def image_reference(self, image_id):
		info = self.image_info[image_id]
		return info['path']

	# function to extract bounding boxes from an annotation file
	def extract_boxes(self,filename):
		tree = ElementTree.parse(filename)
		root = tree.getroot()
		# extract each bounding box
		boxes = list()
		for box in root.findall('.//bndbox'):
			xmin = int(box.find('xmin').text)
			ymin = int(box.find('ymin').text)
			xmax = int(box.find('xmax').text)
			ymax = int(box.find('ymax').text)
			coors = [xmin, ymin, xmax, ymax]
			boxes.append(coors)
		# extract image dimensions
		width = int(root.find('.//size/width').text)
		height = int(root.find('.//size/height').text)
		return boxes, width, height


# define a configuration for the model
class GunConfig(Config):
	# define the name of the configuration
	NAME = "gun_cfg"
	# number of classes (background + kangaroo)
	NUM_CLASSES = 1 + 1
	STEPS_PER_EPOCH = 216



# Prepare dataset
data_train = GunDataset()
data_train.load_dataset('Gun', is_train=True)
data_train.prepare()
print('Length of training dataset: ', len(data_train.image_ids))

data_test = GunDataset()
data_test.load_dataset('Gun', is_train=False)
data_test.prepare()
print('Length of testing datase: ', len(data_test.image_ids))


# define model and pre-fit with mask_rcnn_coco wigths
# learning rate = 0.001
# epochs = 5
config = GunConfig()
model = MaskRCNN(mode='training', model_dir='./config_gun', config=config)
model.load_weights('mask_rcnn_coco.h5', by_name=True, exclude=["mrcnn_class_logits", "mrcnn_bbox_fc",  "mrcnn_bbox", "mrcnn_mask"])
model.train(data_train, data_test, learning_rate=config.LEARNING_RATE, epochs=5, layers='heads')
