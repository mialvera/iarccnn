from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import expand_dims
from numpy import asarray
from mrcnn.utils import Dataset
from mrcnn.visualize import display_instances
from mrcnn.utils import extract_bboxes
import matplotlib.pyplot as plt
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.utils import compute_ap
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image
from numpy import mean
from random import shuffle
from matplotlib.patches import Rectangle


# class that defines and loads the gun dataset
class GunDataset(Dataset):
	# rewrite function to load dataset
	def load_dataset(self, dataset_dir, is_train=True):
		# add class gun dataset
		self.add_class("dataset", 1, "gun")
		# list all images
		for filename in listdir(dataset_dir + '/images/'):
			# get image id, number increasing
			imageId = filename[4:-4]
			if is_train and int(imageId) >= 200:
				continue
			if not is_train and int(imageId) < 200:
				continue
			img = dataset_dir + '/images/' + filename
			ann = dataset_dir + '/annots/' +'gun_' + imageId + '.xml'
			# add to dataset
			self.add_image('dataset', image_id=imageId, path=img, annotation=ann)


	# load the masks for an image
	def load_mask(self, image_id):
		# get info of image
		info = self.image_info[image_id]
		# define box file location
		path = info['annotation']
		# get bounding bixes
		boxes, w, h = self.extract_boxes(path)
		# create one array for all masks, each on a different channel
		masks = zeros([h, w, len(boxes)], dtype='uint8')
		# create masks
		class_ids = list()
		for i in range(len(boxes)):
			box = boxes[i]
			row_s, row_e = box[1], box[3]
			col_s, col_e = box[0], box[2]
			masks[row_s:row_e, col_s:col_e, i] = 1
			class_ids.append(self.class_names.index('gun'))
		return masks, asarray(class_ids, dtype='int32')


	# load an image reference
	def image_reference(self, image_id):
		info = self.image_info[image_id]
		return info['path']

	# function to extract bounding boxes from an annotation file
	def extract_boxes(self,filename):
		# load and parse the file
		tree = ElementTree.parse(filename)
		# get the root of the document
		root = tree.getroot()
		# extract each bounding box
		boxes = list()
		for box in root.findall('.//bndbox'):
			xmin = int(box.find('xmin').text)
			ymin = int(box.find('ymin').text)
			xmax = int(box.find('xmax').text)
			ymax = int(box.find('ymax').text)
			coors = [xmin, ymin, xmax, ymax]
			boxes.append(coors)
		# extract image dimensions
		width = int(root.find('.//size/width').text)
		height = int(root.find('.//size/height').text)
		return boxes, width, height


# define the prediction configuration
class PredictionConfig(Config):
	# define the name of the configuration
	NAME = "gun_cfg"
	# number of classes (background + gun)
	NUM_CLASSES = 1 + 1
	# simplify GPU config
	GPU_COUNT = 1
	IMAGES_PER_GPU = 1

# plot a number of photos with ground truth and predictions
def plot_actual_vs_predicted(dataset, model, cfg, n_images=5):
    # load image and mask
    for i in range(n_images):
        # load the image and mask
        image = dataset.load_image(i)
        mask, _ = dataset.load_mask(i)
        # convert pixel values (e.g. center)
        scaled_image = mold_image(image, cfg)
        # convert image into one sample
        sample = expand_dims(scaled_image, 0)
        # make prediction
        yhat = model.detect(sample, verbose=0)[0]
        # define subplot
        plt.subplot(n_images, 2, i*2+1)
        # plot raw pixel data
        plt.imshow(image)
        plt.title('Actual')
        # plot masks
        for j in range(mask.shape[2]):
            plt.imshow(mask[:, :, j], cmap='gray', alpha=0.3)
        # get the context for drawing boxes
        plt.subplot(n_images, 2, i*2+2)
        # plot raw pixel data
        plt.imshow(image)
        plt.title('Predicted')
        ax = plt.gca()
        # plot each box
        for box in yhat['rois']:
            # get coordinates
            y1, x1, y2, x2 = box
            # calculate width and height of the box
            width, height = x2 - x1, y2 - y1
            # create the shape
            rect = Rectangle((x1, y1), width, height, fill=False, color='red')
            # draw the box
            ax.add_patch(rect)
    # show the figure
    plt.show()


# calculate the mAP for a model on a given dataset
def evaluate_model(dataset, model, cfg):
	APs = list()
	count = 1
	for image_id in dataset.image_ids:
		# load image, bounding boxes and masks for the image id
		#print("imagen #: ", count)
		#print("id: ", image_id)
		imageP = data_train.load_image(image_id)
		# plt.imshow(imageP)
		# plt.show()
		image, image_meta, gt_class_id, gt_bbox, gt_mask = load_image_gt(dataset, cfg, image_id, use_mini_mask=False)
		# convert pixel values (e.g. center)
		scaled_image = mold_image(image, cfg)
		# convert image into one sample
		sample = expand_dims(scaled_image, 0)
		# make prediction
		yhat = model.detect(sample, verbose=1)
		# extract results for first sample
		r = yhat[0]
		# calculate statistics, including AP
		AP, _, _, _ = compute_ap(gt_bbox, gt_class_id, gt_mask, r["rois"], r["class_ids"], r["scores"], r['masks'])
		# store
		#print(AP)
		APs.append(AP)
	# calculate the mean AP across all images
	mAP = mean(APs)
	return mAP

# Prepare dataset
data_train = GunDataset()
data_train.load_dataset('Gun', is_train=True)
data_train.prepare()
print('Length of training dataset: ', len(data_train.image_ids))

data_test = GunDataset()
data_test.load_dataset('Gun', is_train=False)
data_test.prepare()
print('Length of testing datase: ', len(data_test.image_ids))

# create config
cfg = PredictionConfig()

# define the model
model = MaskRCNN(mode='inference', model_dir='./config_gun', config=cfg)
# load model weights after training
model.load_weights('mask_rcnn_gun_cfg_0005.h5', by_name=True)


test_mAP = evaluate_model(data_test, model, cfg)
print("Test average accuracy for test images: %.3f" % test_mAP)

# observe precitions in images
plot_actual_vs_predicted(data_test, model, cfg)
