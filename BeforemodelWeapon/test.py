from __future__ import absolute_import, division, print_function, unicode_literals

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras


# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

import cv2
import os
from random import shuffle
from tqdm import tqdm



train_data = './ImageData/Objects/train'
test_data = './ImageData/Objects/test'

class_names = ['gun', 'knife']

def one_hot_label(img):
	label = img.split('_')[0]
	if(label == 'gun'):
		ohl = np.array([1,0])
	elif(label == 'knife'):
		ohl = np.array([0,1])
	return ohl

def train_data_with_label():
	train_images = []
	for i in tqdm(os.listdir(train_data)):
		path = os.path.join(train_data, i)
		img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
		img = cv2.resize(img, (64,64))
		train_images.append([np.array(img), one_hot_label(i)])
	shuffle(train_images)
	return train_images

def test_data_with_label():
	test_images = []
	for i in tqdm(os.listdir(train_data)):
		path = os.path.join(train_data, i)
		img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
		img = cv2.resize(img, (64,64))
		test_images.append([np.array(img), one_hot_label(i)])
	return test_images


training_images = train_data_with_label()
testing_images = test_data_with_label()
tr_img_data = np.array([i[0] for i in training_images]).reshape(-1,64,64,1)
tr_lbl_data = np.array([i[1] for i in training_images])
tst_img_data = np.array([i[0] for i in testing_images]).reshape(-1,64,64,1)
tst_lbl_data = np.array([i[1] for i in testing_images])


# convulutional model

model = keras.Sequential([
    keras.layers.Flatten(input_shape=[64, 64,1]),
    keras.layers.Dense(128, activation=tf.nn.relu),
	keras.layers.Dropout(0.25),
	keras.layers.Dense(128, activation=tf.nn.relu),
	keras.layers.Dropout(0.25),
    keras.layers.Dense(2, activation=tf.nn.softmax)
#	keras.layers.Dense(128, activation=tf.nn.relu)
])


model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.fit(tr_img_data, tr_lbl_data, epochs=5)

model.summary()

test_loss, test_acc = model.evaluate(tst_img_data, tst_lbl_data)

print('Test accuracy:', test_acc)

predictions = model.predict(tst_img_data)

print(predictions[0])


def plot_image(i, predictions_array, true_label, img):
  predictions_array, true_label, img = predictions_array[i], true_label[i][1], img[i][0]
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])

  plt.imshow(img, cmap='gray')

  predicted_label = np.argmax(predictions_array)
  if predicted_label == true_label:
    color = 'blue'
  else:
    color = 'red'

  plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                100*np.max(predictions_array),
                                class_names[int(true_label)]),
                                color=color)

def plot_value_array(i, predictions_array, true_label):
  predictions_array, true_label = predictions_array[i], true_label[i][1]
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])
  thisplot = plt.bar(range(2), predictions_array, color="#777777")
  plt.ylim([0, 1])
  predicted_label = np.argmax(predictions_array)

  thisplot[predicted_label].set_color('red')
  thisplot[true_label].set_color('blue')

for i in range(len(testing_images)):
	plt.figure(figsize=(6,3))
	plt.subplot(1,2,1)
	plot_image(i, predictions, tst_lbl_data, testing_images)
	plt.subplot(1,2,2)
	plot_value_array(i, predictions,  tst_lbl_data)
	plt.show()
